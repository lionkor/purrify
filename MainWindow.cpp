#include "MainWindow.h"
#include "./ui_MainWindow.h"

#include <QClipboard>
#include <QMessageBox>
#include <QRegularExpression>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);
}

static const char* s_about = R"(
<b>Purrify v1.1.0</b>
<p>Copyright (c) Lion Kortlepel 2022</p>
<a href='http://kortlepel.com'>kortlepel.com</a> | <a href='mailto:development@kortlepel.com'>development@kortlepel.com</a>
)";

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_inputTextEdit_textChanged() {
    QString text = ui->inputTextEdit->toPlainText();
    ui->outputTextEdit->clear();
    ui->progressBar->setMaximum(0);
    QStringList lines = text.split("\n");
    QStringList outputLines {};
    QRegularExpression expr(R"(^((\d){0,2}\:)?(\d){1,2}\:(\d){2}$)", { QRegularExpression::PatternOption::ExtendedPatternSyntaxOption });
    QRegularExpressionMatch match;
    expr.optimize();
    QString outputText;
    outputText.reserve(text.size());
    for (const auto& line : lines) {
        match = expr.match(line);
        if (!match.hasMatch()) {
            outputText.append(line + " ");
        }
    }
    outputText.remove(QRegularExpression(R"(^\s*)"));
    outputText.remove(QRegularExpression(R"(\s*$)"));
    ui->outputTextEdit->setPlainText(outputText);
    ui->progressBar->setMaximum(1);
}

// paste
void MainWindow::on_pushButton_clicked() {
    QClipboard* clipboard = QGuiApplication::clipboard();
    ui->inputTextEdit->setPlainText(clipboard->text());
}

// copy
void MainWindow::on_pushButton_3_clicked() {
    QClipboard* clipboard = QGuiApplication::clipboard();
    clipboard->setText(ui->outputTextEdit->toPlainText());
}

void MainWindow::on_actionAbout_triggered() {
    QMessageBox::about(this, "About Purrify", s_about);
}

void MainWindow::on_actionAlways_on_top_triggered(bool checked) {
    Qt::WindowFlags flags = this->windowFlags();
    if (checked) {
        this->setWindowFlags(flags | Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint);
        this->show();
    } else {
        this->setWindowFlags(flags ^ (Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint));
        this->show();
    }
}
